package com.udemy.springdemo.controllers;
/*
    create 22.05.2018 23:02 by Michal
*/

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//@RestController
public class ErrorSendingMailController {

    private static final String ERROR_MESSAGE = "Ups. Something goes wrong.";

    @RequestMapping("/error")
    public String errorMessage(){
        return ERROR_MESSAGE;
    }
}
