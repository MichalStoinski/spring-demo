package com.udemy.springdemo.controllers;
/*
    create 21.05.2018 19:42 by Michal
*/

import com.udemy.springdemo.mail.MailSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.MessagingException;

@RestController // put bean into Application Context
public class MailController {

    private MailSender mailSender;

    private static final String SUCCESS = "Mail sent successfully";
    private int i = 0;

    @Autowired // it's not necessary here - by default constructor is autowired
    public MailController(MailSender mailSender) {
        this.mailSender = mailSender;
    }

    @RequestMapping("/mail")
    public String mail() throws MessagingException {
        mailSender.send("michal.stoinski@gmail.com", "[" + i++ + "] Test mail", "It's only a test");
        return SUCCESS;
    }
}
