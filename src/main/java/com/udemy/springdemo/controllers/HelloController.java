package com.udemy.springdemo.controllers;
/*
    create 21.05.2018 19:30 by Michal
*/

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @Value("${app.env}")
    private String appEnv;

    @RequestMapping("/hello")
    public String Hello(){
        return "Hello, world! [" + appEnv + "]";
    }
}
