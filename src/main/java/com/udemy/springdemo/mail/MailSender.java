package com.udemy.springdemo.mail;
/*
    create 21.05.2018 19:35 by Michal
*/

import javax.mail.MessagingException;

public interface MailSender {

    void send(String to, String subject, String body) throws MessagingException;
}
