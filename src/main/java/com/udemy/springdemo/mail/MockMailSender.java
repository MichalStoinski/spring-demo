package com.udemy.springdemo.mail;
/*
    create 21.05.2018 19:36 by Michal
*/

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

//@Component // remove, while adding configuration class
//@Component // creates object of this class (a bean) and put it into Application Context
public class MockMailSender implements MailSender {

    private static Log log = LogFactory.getLog(MockMailSender.class);

    @Override
    public void send(String to, String subject, String body) {
        log.info("Sending MOCK mail to: " + to);
        log.info("with subject: " + subject);
        log.info("and body: " + body);
    }
}
