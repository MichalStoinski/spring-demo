package com.udemy.springdemo.mail;
/*
    create 23.05.2018 09:25 by Michal
*/

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class DemoBean {

    public static final String INFO_DEMO_BEAN = "===== Demo bean created =====";
    private static Log log = LogFactory.getLog(DemoBean.class);

    public DemoBean() {
        log.info(INFO_DEMO_BEAN);
    }
}
