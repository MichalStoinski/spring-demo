package com.udemy.springdemo.mail;
/*
    create 22.05.2018 11:36 by Michal
*/

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.mail.javamail.JavaMailSender;

/**
 * Configuration class substitutes the @Component annotation creating beans
 * Supposing we can't add that annotation to those classes (e.g they are packed into jar file)
 */
@Configuration
public class MailConfig {

    @Bean
    public DemoBean demoBean(){
        return new DemoBean();
    }

    @Bean
//    @Profile("dev")
    @ConditionalOnProperty(name = "spring.mail.host", havingValue = "6108273142060426fcafe8fc", matchIfMissing = true)
    public MailSender mockMailSender() {
        return new MockMailSender();
    }

    @Bean
    @ConditionalOnProperty("spring.mail.host")
    public MailSender smtpMailSender(JavaMailSender javaMailSender) {
        demoBean(); // no catching the result, just only call the method
        return new SmtpMailSender(javaMailSender);
    }
}
