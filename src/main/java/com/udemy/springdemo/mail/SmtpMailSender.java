package com.udemy.springdemo.mail;
/*
    create 21.05.2018 19:36 by Michal
*/

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Primary;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

//@Component // remove, while adding configuration class
//@Component // creates object of this class (a bean) and put it into Application Context
//@Primary // this bean get preference for the injection
public class SmtpMailSender implements MailSender {

    private static Log log = LogFactory.getLog(SmtpMailSender.class);
    private JavaMailSender javaMailSender;

    public SmtpMailSender(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Override
    public void send(String to, String subject, String body) throws MessagingException {

        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true);
        helper.setTo(to);
        helper.setSubject(subject);
        helper.setText(body, false); // true indicates html

        javaMailSender.send(message);

        log.info("Sending SMTP mail to: " + to);
        log.info("with subject: " + subject);
        log.info("and body: " + body);
    }
}
